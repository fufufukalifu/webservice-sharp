﻿using System;
using System.Web;
using System.Web.Services;

using System.Data;
using System.Data.SqlClient;
namespace webservice
{
    [WebService(Namespace = "http://products.com/webservices/",
            Description = "Webservice Tugas Akhir")]
    
    public class Service : System.Web.Services.WebService
    {
        public SqlConnection connection = new SqlConnection("Server=localhost;Database=webservice_commerce;" +
                                                            "Uid=root;Pwd=root;");  

        [WebMethod]
        public String firstMethod(String input)
        {
           return input + " : " + DateTime.Now.ToString() + " Input was changed";
        }

        public DataSet GetCustomers()  
        {  
            SqlDataAdapter adapter = new SqlDataAdapter(  
              "SELECT * FROM Products", connection);  

            DataSet products = new DataSet();  
            adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey;  
            adapter.Fill(products, "Customers");  

            return products;  
          } 
    }
}